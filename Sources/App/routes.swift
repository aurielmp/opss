import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // "It works" page
    router.get { req -> Future<Response> in
        let dirs: DirectoryConfig = try req.make()
        return try req.streamFile(at: dirs.workDir + "Public/index.html")
    }
    
    // Says hello
    router.get("hello", String.parameter) { req -> Future<View> in
        return try req.view().render("hello", [
            "name": req.parameters.next(String.self)
        ])
    }

    let formController = FormController()   

    router.post("handleForm", use: formController.index)
}
