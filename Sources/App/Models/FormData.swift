import Vapor

struct FormData: Content {
  let firstName: String
  let lastName: String
  let recipientEmail: String
  let subject: String
  let message: String
}
