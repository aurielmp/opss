import Vapor
import Mailgun

protocol Sendable {
  func sendEmail(on req: Request) throws -> Future<Response>
}

extension FormData: Sendable {
  func sendEmail(on req: Request) throws -> Future<Response> {
    let mailgun = try req.make(Mailgun.self)

    return try req.view().render("Emails/FormEmail", [
        "firstName": self.firstName,
        "lastName": self.lastName,
        "recipientEmail": self.recipientEmail,
        "subject": self.subject,
        "message": self.message,
    ]).map { view in
      String(bytes: view.data, encoding: .utf8) ?? ""
    }.map { html in 
      Mailgun.Message(
        from: self.recipientEmail,
        to: "login.hawk@gmail.com",
        subject: "[Form message] \(self.subject)",
        text: "",
        html: html
      )
    }.flatMap(to: Response.self) { message in
        try mailgun.send(message, on: req)
    }.catchFlatMap { error in
        throw Abort(.internalServerError, reason: "Problem with sending email.")
    }
  }
}
