import Validation

extension FormData: Validatable, Reflectable {
  static func validations() throws -> Validations<FormData> {
    var validations = Validations(FormData.self)
    try validations.add(\.firstName, .alphanumeric && .count(2...))
    try validations.add(\.lastName, .alphanumeric && .count(2...))
    try validations.add(\.recipientEmail, .email)
    try validations.add(\.subject, .count(5...))
    try validations.add(\.message, .count(15...))
    return validations
  }
}
