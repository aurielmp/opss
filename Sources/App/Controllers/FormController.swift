import Vapor

final class FormController {
  func index(_ req: Request) throws -> Future<Response> {
    return try req.content.decode(FormData.self).flatMap(to: Response.self) { form in
      try form.validate()
      
      return try form.sendEmail(on: req)
    }
  }
}

