import Leaf
import Vapor
import Mailgun

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    // Register providers first
    try services.register(LeafProvider())

    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)
    
    // Use Leaf for rendering views
    config.prefer(LeafRenderer.self, for: ViewRenderer.self)

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    let apiKey = Environment.get("MAILGUN_API_KEY") ?? ""
    let domainName = Environment.get("MAILGUN_DOMAIN_NAME") ?? ""
    let mailgun = Mailgun(apiKey: apiKey, domain: domainName, region: .us)
    services.register(mailgun, as: Mailgun.self)
}
