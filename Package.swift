// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "studia-opss_backend",
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),

        // 🍃 An expressive, performant, and extensible templating language built for Swift.
        .package(url: "https://github.com/vapor/leaf.git", from: "3.0.0"),
        .package(url: "https://github.com/vapor/validation.git", from: "2.0.0"),
        .package(url: "https://github.com/twof/VaporMailgunService.git", from: "1.5.0")
    ],
    targets: [
        .target(name: "App", dependencies: ["Leaf", "Vapor", "Validation", "Mailgun"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

